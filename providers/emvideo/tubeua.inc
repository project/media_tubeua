<?php
/*
 * @file tubeua.inc
 * Provides tubeua integration for emvideo module
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v3 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 * 
 */

/**
 *  This is the main URL for your provider.
 */
define('EMVIDEO_TUBEUA_MAIN_URL', 'http://www.tube.ua/');

/**
 *  This defines the version of the content data array that we serialize
 *  in emvideo_tubeua_data(). If we change the expected keys of that array,
 *  we must increment this value, which will allow older content to be updated
 *  to the new version automatically.
 */
define('EMVIDEO_TUBEUA_DATA_VERSION', 1);

/**
 * hook emvideo_PROVIDER_info
 * This returns information relevant to a specific 3rd party video provider.
 *
 * @return
 *   A keyed array of strings requested by various admin and other forms.
 *    'provider' => The machine name of the provider. This must be the same as
 *      the base name of this filename, before the .inc extension.
 *    'name' => The translated name of the provider.
 *    'url' => The url to the main page for the provider.
 *    'settings_description' => A description of the provider that will be
 *      posted in the admin settings form.
 *    'supported_features' => An array of rows describing the state of certain
 *      supported features by the provider. These will be rendered in a table,
 *      with the columns being 'Feature', 'Supported', 'Notes'. In general,
 *      the 'Feature' column will give the name of the feature, 'Supported'
 *      will be Yes or No, and 'Notes' will give an optional description or
 *      caveats to the feature.
 */
function emvideo_tubeua_info() {
  $features = array(
    array(t('RSS Attachment'), t('Yes'), ''),
    array(t('Thumbnails'), t('Yes'), t('')),
    array(t('Full screen mode'), t('Yes'), t('You may customize the player to enable or disable full screen playback. Full screen mode is enabled by default.')),
  );
  return array(
    'provider' => 'tubeua',
    'name' => t('Tube.ua'),
    'url' => EMVIDEO_TUBEUA_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from !tubeua.', array('!tubeua' => l(t('Tube.ua'), EMVIDEO_TUBEUA_MAIN_URL))),
    'supported_features' => $features,
  );
}

/**
 *  hook emvideo_PROVIDER_settings
 *  This should return a subform to be added to the emvideo_settings() admin
 *  settings page.
 *
 *  Note that a form field set will already be provided at $form['tubeua'],
 *  so if you want specific provider settings within that field set, you should
 *  add the elements to that form array element.
 */
function emvideo_tubeua_settings() {
  // We'll add a field set of player options here. You may add other options
  // to this element, or remove the field set entirely if there are no
  // user-configurable options allowed by the tubeua provider.
  $form['tubeua']['player_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Embedded video player options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // This is an option to set the video to full screen. You should remove this
  // option if it is not provided by the tubeua provider.
  $form['tubeua']['player_options']['emvideo_tubeua_full_screen'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow fullscreen'),
    '#default_value' => variable_get('emvideo_tubeua_full_screen', 1),
    '#description' => t('Allow users to view video using the entire computer screen.'),
  );

  return $form;
}

/**
 *  hook emvideo_PROVIDER_extract
 *
 *  This is called to extract the video code from a pasted URL or embed code.
 *
 *  We'll be passed a URL or the embed code from a video when an editor pastes
 *  that in the field's textfield. We'll need to either pass back an array of
 *  regex expressions to match, or do the matching ourselves and return the
 *  resulting video code.
 *
 *  @param $parse
 *    An optional string with the pasted URL or embed code.
 *  @return
 *    Either an array of regex expressions to be tested, or a string with the
 *    video code to be used. If the hook tests the code itself, it should
 *    return either the string of the video code (if matched), or an empty
 *    array. Otherwise, the calling function will handle testing the embed code
 *    against each regex string in the returned array.
 */
function emvideo_tubeua_extract($parse = '') {
  // Here we assume that a URL will be passed in the form of
  // http://www.tube.ua/video/text-video-title
  // or embed code in the form of <object value="http://www.tube.ua/embed...".

  // We'll simply return an array of regular expressions for Embedded Media
  // Field to handle for us.
  /*
   our embed scripts are
   <object width="608" height="342" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000">
   <param name="movie" value="http://tube.ua/tubeplay.swf?key=186c76b1c8d7c90d8d67&amp;absPth=http%3A%2F%2Ftube.ua%2Fget_player_vars.php" ></param>
   <param name="allowFullScreen" value="true"></param>
   <param name="allowScriptAccess" value="sameDomain" ></param>
   <embed src="http://tube.ua/tubeplay.swf?key=186c76b1c8d7c90d8d67&amp;absPth=http%3A%2F%2Ftube.ua%2Fget_player_vars.php"
   type="application/x-shockwave-flash"
   allowScriptAccess="sameDomain"
   allowFullScreen="true" width="608" height="342"></embed></object>
   or our urls are
   http://tube.ua/_custom_channel.php?chid=1390&viewkey=186c76b1c8d7c90d8d67
   */
  return array(
    // In this expression, we're looking first for text matching the expression
    // between the @ signs. The 'i' at the end means we don't care about the
    // case. Thus, if someone enters http://www.Tube.ua.com, it will still
    // match. We escape periods as \., as otherwise they match any character.
    // The text in parentheses () will be returned as the provider video code,
    // if there's a match for the entire expression. In this particular case,
    // ([^?]+) means to match one more more characters (+) that are not a
    // question mark ([^\?]), which would denote a query in the URL.
    '@tube\.ua/_custom_channel\.php\?chid=[0-9]+&viewkey=(.*)@i',

    // Now we test for embedded video code, which is similar in this case to
    // the above expression, except that we can safely assume we won't have a
    // query in the URL, and that the URL will be surrounded by quotation marks,
    // and have /embed/ rather than /video/ in the URL. Note that regular
    // expressions will be tested for matches in the order provided, so you
    // may need to move this value above the other in some cases. Obviously,
    // in the case of this tubeua provider, you could easily improve the
    // regular expression to match against either a URL or embed code with
    // one expression, such as '@tubeua\.com/[watch|embed]/([^"\?]+)@i'.
    // However, many other providers have more complex requirements, so
    // we split them up for this demonstration.
    '@tube\.ua/tubeplay\.swf\?key=([^&]+)@i',
  );
}

/**
 *  hook emvideo_PROVIDER_data
 *
 *  Provides an array to be serialised and made available with $item elsewhere.
 *
 *  This data can be used to store any extraneous information available
 *  specifically to the tubeua provider.
 */
function emvideo_tubeua_data($field, $item) {
  // Initialize the data array.
  $data = array();

  // Create some version control. Thus if we make changes to the data array
  // down the road, we can respect older content. If allowed by Embedded Media
  // Field, any older content will automatically update this array as needed.
  // In any case, you should account for the version if you increment it.
  $data['emvideo_tubeua_version'] = EMVIDEO_TUBEUA_DATA_VERSION;
  
  $url = 'http://tube.ua/_custom_channel.php?chid=&viewkey='. $item['value'];

  $cache_id = 'media_tubeua_'. $item['value'];
  $cache = cache_get($cache_id);
  if ($cache && $cache->data) {
    $html = $cache->data;
  }
  else {
    $result = drupal_http_request($url);
    $code   = floor($result->code / 100) * 100;
    $types  = array('text/html');
    if ($result->data && $code != 400 && $code != 500 && in_array($result->Content-Type, $types)) {
      cache_set($cache_id, $result->data);
      $html = $result->data;
    }
    else  {
      //if we are unsuccessful then log a message in watchdog
      watchdog('media_tubeua', 'The html '. $url .' could not be retrieved');
      return $data;
    }    
  }
  
  $matches = array();
  $result = preg_match_all('@rel="image_src" href="http://tube\.ua/thumb/(.+)?\.jpg@', $html, $matches);
  
  if (!$result) {
    return $data;
  }
  
  $data['thumbnail'] = 'http://tube.ua/thumb/'. $matches[1][0] .'.jpg';
  return $data;
}

/**
 *  hook emvideo_PROVIDER_rss
 *
 *  This attaches a file to an RSS feed.
 */
function emvideo_tubeua_rss($item, $teaser = NULL) {
  if ($item['value']) {
    $file['thumbnail']['filepath'] = $item['data']['thumbnail'];

    return $file;
  }
}

/**
 * hook emvideo_PROVIDER_embedded_link($video_code)
 * returns a link to view the video at the provider's site.
 *  @param $video_code
 *    The string containing the video to watch.
 *  @return
 *    A string containing the URL to view the video at the original provider's site.
 */
function emvideo_tubeua_embedded_link($video_code) {
  return 'http://www.tube.ua/_custom_channel.php\?chid=+&viewkey='. $video_code;
}

/**
 * The embedded flash displaying the tubeua video.
 */
function theme_emvideo_tubeua_flash($item, $width, $height, $autoplay) {
  $output = '';
  if ($item['embed']) {
    /*
     <object width="608" height="342" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000">
   <param name="movie" value="http://tube.ua/tubeplay.swf?key=186c76b1c8d7c90d8d67&amp;absPth=http%3A%2F%2Ftube.ua%2Fget_player_vars.php" ></param>
   <param name="allowFullScreen" value="true"></param>
   <param name="allowScriptAccess" value="sameDomain" ></param>
   <embed src="http://tube.ua/tubeplay.swf?key=186c76b1c8d7c90d8d67&amp;absPth=http%3A%2F%2Ftube.ua%2Fget_player_vars.php"
   type="application/x-shockwave-flash"
   allowScriptAccess="sameDomain"
   allowFullScreen="true" width="608" height="342"></embed></object>*/
    $autoplay = $autoplay ? 'true' : 'false';
    $fullscreen = variable_get('emvideo_tubeua_full_screen', 1) ? 'true' : 'false';
    $output = '<object width="'. $width .'" height="'. $height .'" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000">
      <param name="movie" value="http://tube.ua/tubeplay.swf?key='. $item['value'] .'&amp;absPth=http%3A%2F%2Ftube.ua%2Fget_player_vars.php" ></param>
      <param name="allowFullScreen" value="'. $fullscreen .'"></param>
      <param name="allowScriptAccess" value="sameDomain" ></param>
      <embed src="http://tube.ua/tubeplay.swf?key='. $item['value'] .'&amp;absPth=http%3A%2F%2Ftube.ua%2Fget_player_vars.php"
      type="application/x-shockwave-flash"
      allowScriptAccess="sameDomain"
      allowFullScreen="true" width="'. $width .'" height="'. $height .'"></embed></object>';
  }
  return $output;
}

/**
 * hook emvideo_PROVIDER_thumbnail
 * Returns the external url for a thumbnail of a specific video.
 *  @param $field
 *    The field of the requesting node.
 *  @param $item
 *    The actual content of the field from the requesting node.
 *  @return
 *    A URL pointing to the thumbnail.
 */
function emvideo_tubeua_thumbnail($field, $item, $formatter, $node, $width, $height) {
  // In this demonstration, we previously retrieved a thumbnail using oEmbed
  // during the data hook.
  return $item['data']['thumbnail'];
}

/**
 *  hook emvideo_PROVIDER_video
 *  This actually displays the full/normal-sized video we want, usually on the
 *  default page view.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_tubeua_video($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('emvideo_tubeua_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 *  hook emvideo_PROVIDER_video
 *
 *  This actually displays the preview-sized video we want, commonly for the
 *  teaser.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_tubeua_preview($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('emvideo_tubeua_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 *  Implementation of hook_emfield_subtheme.
 *  This returns any theme functions defined by this provider.
 */
function emvideo_tubeua_emfield_subtheme() {
  $themes = array(
      'emvideo_tubeua_flash'  => array(
          'arguments' => array('item' => NULL, 'width' => NULL, 'height' => NULL, 'autoplay' => NULL),
          'file' => 'providers/tubeua.inc',
          // If you don't provide a 'path' value, then it will default to
          // the emvideo.module path. Obviously, replace 'emtubeua' with
          // the actual name of your custom module.
          'path' => drupal_get_path('module', 'media_tubeua'),
      )
  );
  return $themes;
}
